"eGen Bangla Sentiment Analyzer" is a product of eGeneration Ltd, Dhaka Bangladesh.
It has used advanced machine learning techniques developed by NLP Team of eGeneration Ltd.

How to use:
1. Download and Run "Sentiment_test_model.exe"
 
2. Put Bangla text in the text field. (Example: "খুব ভালো হয়েছে" ) 

3. Press "Check Sentiment" Button.

Developed by, eGeneration NLP Team
Lead Engineer: Ishteaque Alam  
E-mail: ishteaque.alam@egeneration.co 
